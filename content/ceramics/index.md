---
title: Ceramics 
description: Here is a collection showcasing some ceramics stuff I have made as well as a short (opiniated) explanation on the work process.
---

<button class="btn enc-btn" onclick="showBackground()">Show/hide description of ceramics workflow </button>

<div id="background" style="display:none">

## Notes

- One must use water, which when mixed with clay results in "slip" or "clay slurry", to mold and shape the clay.
- One can only work with the clay on the wheel as long as it is not too wet.
- Controlling the amount of water in the clay is essential from start to finish. 

## Making the object

1. Wedge the clay. This is in order to push out any eventual small amounts of air and to push out moisture, allowing you to work longer on the wheel (as at some point, the object will collapse from being too wet to hold a shape).
2. Center clay on wheel. Quite difficult to get perfectly right, sometimes 95% centered is "good enough". If not centered enough, everything fails. 
3. When making anything but plates, make a cylinder. This is done by making a hole in the centered clay and pulling the clay out and then "up". The pull up is basically done by pressuring an equal amount (no more or less, which makes it difficult) on the wall of the cylinder and carefully pulling the clay up. 
4. When a cylinder is made, one can shape it to whatever form is desired; a tall vase with a curve, a wide bowl for a salad, a mug or cup for coffee or a pot for flowers. 
5. The object is removed from the wheel with a simple steel wire. Usually the object is too wet to move without fatally changing the shape, so one can dry it with a blowdryer or heatgun before moving.  
6. Time (for me atleast) so far: 20-40 minutes depending on the size and shape of the object. 
7. The object is left to dry until the clay has a leathery feel, usually a day or two depending on the humidity and temperature of the room. 
8. While the general shape is done, the bottom of the object is usually uneven and not very pretty. This is fixed by placing it upside down on the wheel and trimming/shaping the bottom with various tools. Getting the object fixed on the center of the wheel can be quite difficult (especially if the object is not symmetric enough). This usually takes 10-15 minutes.
9. The object is left to dry further for a week (or much, much longer, if your kiln is non-functional) until completely dry.

## Firing and glazing the object

1. The object is "bisque fired" in a kiln. This takes about 12 hours and the kiln temperature is about 950 degrees Celsius. This removes the water completely (I think), allowing it to soak up water. 
2. The object can now be glazed. It is not required, but it does add a nice touch of color. The easiest way it to dip it in a bucket of glaze, which has about the same consistency as paint (it is just glaze powder mixed with the correct amount of water). We want an even, not too thick layer of glaze that does not extend to the bottom (as it will basically glue the object to the kiln, see picture below). The most difficult part of this step is really to pick the right glaze.
3. The object is now "glaze fired" (in the same kiln). Depending on the glaze, this can take up to 24 hours and the kiln temperature is 1050 to 1260 degrees Celsius. 
4. The process is done and the object is hardened and watertight. 

Total time per object: about an hour. 

</div>

## Gallery of finished items (click to enlargen)

The following images are copyright &#169; 2022 - present Philip Jakobsen.

{{< gallery match="img/finished/*" sortOrder="asc" rowHeight="250" margins="10" >}}

## Gallery of non-finished and/or broken items

{{< gallery match="img/in_progress/*" sortOrder="desc" rowHeight="250" margins="10" >}}
