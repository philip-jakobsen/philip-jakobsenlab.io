---
title: ConsistentYR  
description: Late 2021 - present
---

Exploring the self-consistency of [YR](https://yr.no), the national norwegian weather service with regards to amount of _predicted_ rainfall. Under development. 
