---
title: ROTMO - ROtating MOlecular Orbitals
description: Summer 2020
---

Source code and longer description can be found at [https://gitlab.com/philip-jakobsen/rotmo](https://gitlab.com/philip-jakobsen/rotmo).

This program is the result of my master's project at Aarhus University in computational chemistry under assoc. prof. Frank Jensen and prof. Jeppe Olsen, for which I received the highest possible grade (12 on the Danish scale). It is written in (modern) Fortran and parallelized with openMP.

A copy of my thesis titled "Representing Exact Electron Densities by a Single Determinant Wave Function" can be found [here](https://gitlab.com/philip-jakobsen/rotmo/-/blob/master/master.pdf).

The project resulted in an article published in the [Journal of Chemical Theory and Computation, JCTC 2021, 17.1 269-276](https://pubs.acs.org/doi/10.1021/acs.jctc.0c01029).

For the version of ROTMO specifically used in the above article see [https://gitlab.com/philip-jakobsen/ROTMO-pub](https://gitlab.com/philip-jakobsen/ROTMO-pub).

## Very short description for "normal" people

An "exact" numerical treatment of the electronic Schrödinger equation is only computationally possible for very small systems (less than say, 10 electrons) due to the rather unforgiving factorial scaling (which is even worse than exponential). This is essentially due to all the electrons interacting with each other, since they are negatively charged. In practice, quantum chemical calculations are done with approximate methods, which are "good enough" for the chosen problem based on an accuracy versus computational time tradeoff. A simple approximation is replacing the electron-electron interaction with a mean field, so each electron only "feels" the average of all the other electrons.

This project concerns the following question: given an exact solution, which can be described in terms of an electron density, how close can an approximate solution be optimized to "match" the exact one by minimizing the difference in electron density.

__Competencies developed:__ computational chemistry, Fortran programing, High Performance Computing, numerical optimization and parallelization. 
