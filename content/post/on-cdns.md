---
title: CDNs and to CDN or not to CDN
date: 2022-09-21
---

A CDN (Content Delivery Network) is basically the idea of getting someone else to host your content, which could be anything really. 

After reading up on the subject of whether or not to use a CDN to serve third pary javascript libraries (plus css), it seems it boils down to the following. 

## For 

- From a developer perspective, it is easier to just point to a url instead of dealing with a slight hassle of downloading, unpacking and making sure everything is at the correct place, such as when trying out different libraries for an image gallery or changing the version of a library.
- The CDN will most likely have a much better geographical presence, thus making the site load faster when the users are not near the site's main server.
- Saving bandwidth by not having to serve said javascript and css. Although the files are small in comparison to images and/or videos, it'll add up. 
- In the past (I think) it was possible for browsers to cache common libraries between different sites (if both siteA and siteB used jquery, for instance), but this is now not possible. 


## Against

- CDNs are an additional point of failure you cannot control, although the risk of a large CDN such as Cloudflare's, Google's or AWS's going down anytime soon is quite small. 
- CDNs are an additional point of potential security holes, potentially allowing remote code execution. This can be remedied by using [subresource integrity](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity) (SRI).
- CDNs could in theory be used for tracking
- Centralising much of the internet infrastructure on a few big players such as Google, Amazon, Microsoft or Cloudflare goes against the original idea of an open, free and somewhat anarcistic web.


## The conclusion

For now, I will be using Cloudflare's CDN [cdnjs](https://cdnjs.com/) for serving jquery, bootstrap and mathjax while using abovementioned SRI to ensure integrity of third party files.
I plan to add a fallback to locally served libraries, mostly to stay independent and for users with third party javascript blocked.
Fonts are served locally to avoid potential tracking by Google.


## Links


[Public CDNs Are Useless and Dangerous](https://httptoolkit.tech/blog/public-cdn-risks/) plus [discussion on Hacker News](https://news.ycombinator.com/item?id=31679872).

[Remote code execution in cdnjs of Cloudflare](https://blog.ryotak.me/post/cdnjs-remote-code-execution-en/) plus [discussion on Hacker News](https://news.ycombinator.com/item?id=27853434).

[The CDN is Dead, Long Live the CDN! - Cache Partitioning in Firefox and Chrome](https://www.peakhour.io/blog/cache-partitioning-firefox-chrome/).


