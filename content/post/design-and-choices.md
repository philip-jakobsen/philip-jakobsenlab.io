---
title: Design and other choices
date: 2022-09-21
---

I want the site to be lean, fast, and responsive. 

- A modified version of the [Minimal](https://github.com/calintat/minimal) theme is used.
- No cookies, tracking or analytics.
- Third party libraries/frameworks are kept at a minimum and most are loaded from the [CloudFlare JavaScript CDN](https://cdnjs.com/libraries/). These are [jquery](https://github.com/jquery/jquery) and [bootstrap 3](https://getbootstrap.com/docs/3.3/).
- The [Open Sans font](https://www.opensans.com/) is used, but served locally from the site and not from Google Fonts (which does some fingerprinting).
- [MathJax](https://www.mathjax.org) is used for typesetting math when needed.
- Some tweaking was needed to make the site look good on mobile devices, especially going from one to two columns in the drop-down navigation menu as well as various small changes.
- The one fancy thing is the color changing thin top and bottom bar, going from blue to very dark blue during the week.
- The image gallery is made with [hugo-shortcode-gallery](https://github.com/mfg92/hugo-shortcode-gallery).
- A QR code can be generated which points to the current site, made with [qrcodejs](https://github.com/davidshimjs/qrcodejs). This requires the `.getScript()` function from jquery, which prevents us from using the slim version of jquery.

This results in a pretty good score on the [web.dev](https://web.dev/measure/) page quality measure of 97, 96 and 100 of the performance, accessibility and best practices categories, respectively. 

