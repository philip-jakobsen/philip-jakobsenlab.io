---
title: About
description: Me and the site
---

## Me

I am currently employed as a DevOps Engineer at [InCommodities](https://incommodities.com/), a power and gas commodities trading company located in Aarhus, Denmark. 
Previously I have held roles as DevOps Engineer at [NNIT](https://www.nnit.com/) and IT consultant at [Odense Kommune](https://www.odense.dk/om-kommunen/forvaltninger/borgmesterforvaltningen/oekonomi-digitalisering/digitalisering-og-data) (where I did both DevOps as well as all sorts of things).

I have a Master's degree in chemistry, with a heavy focus on computational chemistry, which was my actual entry to programming, data analysis, Linux and high-performance computing.

In my spare time I like to make ceramics, read novels, work out at the gym, cook nice food and tinker with various IT-related projects. 

## The site

The site is currently hosted for free on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) with a custom domain.
In the future it will be hosted on a Raspberry Pi from my home (when I get a new router and a static IP). 

It is built with the [Hugo](https://gohugo.io) static site generator (SSG). The first version of the site was just pure handwritten HTML, which got cumbersome and awkward pretty fast. Hugo (and most SSGs) allow me to write content in markdown and makes the layout quite a bit nicer. 

See the posts [design and other choices]( {{< ref "post/design-and-choices.md" >}}) and [on CDNs and to CDN or not to CDN]({{< ref "post/on-cdns.md" >}}) for more information. 

### Known bugs

- MathJax rendering on Safari gives jumpy baseline for subscripts for some weird reason I cannot figure out, I suspect a bug in WebKit. 


## My fiance's cat, Erika

{{< gallery match="img/*" sortOrder="desc" rowHeight="250" margins="10" >}}

