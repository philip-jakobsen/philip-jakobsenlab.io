---
title: Projects
---

This is a list containg a short description of the project on this site.

### [IceCold]({{< ref "/icecold" >}} "IceCold")

Simple calculator to figure out how long you should put a beer can or wine bottle in the freezer in order to achieve the desired temperature.

### [ROTMO]({{< ref "/rotmo" >}} "ROTMO")

My masters's project, a molecular orbital optimizer targeting single-determinant wavefunctions to represent an exact wavefunction. 

### [JuliaFract]({{< ref "/juliafract" >}} "JuliaFract")

Visualize fractals of the Julia type in the Julia programming language. Under development. 

### [ETHARB]({{< ref "/etharb" >}} "ETHARB")

Exploring Ethereum-fiat arbitrage trading oportunities on the Kraken cryptocurrency exchange when funds run dry, especially the ETH-EUR pair on weekends. Under development. 

### [ConsistentYR]({{< ref "/consistent-yr" >}} "ConsistentYR")

Exploring the consistency of YR, the national norwegian weather service with regards to amount of _predicted_ rainfall. Under development. 

### [Ceramics]({{< ref "/ceramics" >}} "Ceramics")

Showcasing some of my ceramics as well as explaining the process.
