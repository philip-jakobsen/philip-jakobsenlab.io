---
title: IceCold - calculator for cooling beverages
description: "Summer 2021"
mathjax: true
---

__Competencies developed__: HTML, CSS and JavaScript. Also elementary thermodynamics and surface/volume integrals.

### Description

This is a simple calculatur to figure out how long a soda bottle, beer can or wine bottle should be left in the fridge or freezer in order to achieve a desired temperature. Input the starting temperature of the liquid, desired temperature, temperature of the fridge/freezer as well as the container size, alcohol percentage and container material.

__Note:__ the following assumes a "perfect" freezer that always keeps the same temperature and the heat exchange _from_ the container _to_ the sorroundings is therefore neglected. This sounds reasonable enough, as long as the starting temperature of the liquid is near room temperature. Due to this, the cooling time is independent of the _number_ of containers; 3 cans of beer will cool just as fast as 1 can. 


<button class="btn enc-btn" onclick="showBackground()">Show/hide theoretical background</button>

<div id="background" style="display:none">

## Theoretical background

The formula needed to calculate the time \\(t\\) (SI unit: \\(\text{s}\\)) it takes for a container with liquid to cool by a specified number of degrees \\(T\\) is Newton's cooling law: 

$$
\begin{align} 
t &= \log{ \left( \frac{T_{\text{desired}} - T_{\text{freezer}}}{(T_{\text{start}} - T_{\text{freezer}}} \right)} / k \\\
k &= h \cdot A / C
\end{align}
$$

Here the surface area \\(A\\) (SI unit: \\(\text{m}^2\\)) is rather tricky to estimate accurately.
Due to this, all containers are approximated as simple cylinders (see the HTML source for the height/width values). 


The constant \\(h\\) (SI unit: \\(\text{W}/(\text{m}^{2}\text{K})\\)) is the _heat transfer coefficient_, which is even more tricky to determine, at least in theory. 
It basically describes how efficient a material is at distributing heat. 
It is assumed independent of temperature (the thermodynamical properties of the container's material does not change during the cooling, which makes sense as long as we're just using normal house-hold freezers).
The values used here for aluminum, glass and plastic are reverse-engineered from [OmniCalculator](https://www.omnicalculator.com/food/chilled-drink). 
There is no distinction between a thick and a thin glass bottle for instance. 



The constant \\(C\\) (SI unit: \\( \text{J}/(\text{kg} \cdot \text{K})\\)) is the _specific heat capacity_ of the liquid.
Here it is assumed to be water plus some fraction of alcohol (which can be zero).
We also assume a density of 1 kg per liter liquid for simplicity (rather good since the liquids are mostly water anyway and alcohol is not _that_ much lighter). 

</div>

{{< include-html "content/icecold/src/form.html" >}}

<span id="freeze_time"></span> 

__Disclaimer:__ I take no responsibility for exploded bottles in your freezer. 
