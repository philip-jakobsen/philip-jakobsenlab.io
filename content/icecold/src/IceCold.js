let freeze_time;

function parse_input() {

    // parse input

    var start_temp = parseFloat(document.getElementsByName("start_temp")[0].value);         // in celcius
    var end_temp = parseFloat(document.getElementsByName("end_temp")[0].value);             // in celsius
    var freezer_temp = parseFloat(document.getElementsByName("freezer_temp")[0].value);     // in celsius
    
    var volume = parseFloat(document.getElementsByName("container")[0].value.split(",")[0]);              // in mL
    var area = parseFloat(document.getElementsByName("container")[0].value.split(",")[1]);                // in m^2

    var alcohol_percent = parseFloat(document.getElementsByName("alcohol_percent")[0].value);

    var container_material = document.querySelector('input[name="container_material"]:checked').value

    console.log("Parsing values")
    console.log("####################################")
    console.log("start_temp: " + start_temp)
    console.log("end_temp: " + end_temp)
    console.log("freezer_temp: " + freezer_temp)
    console.log("volume: " + volume)
    console.log("area: " + area)
    console.log("alcohol_percent: " + alcohol_percent)
    console.log("container_material: " + container_material)
    // console.log("item_multiplier: " + item_multiplier)
    console.log("####################################")

    return {start_temp, end_temp, freezer_temp, volume, area, alcohol_percent, container_material};

}

function calc_heat_capacity(alcohol_percent) {
    
    var alcohol_percent = arguments[0]

    var water_heat_capacity = 4182.0

    var ethanol_heat_capacity = 2570.0 // https://www.engineeringtoolbox.com/specific-heat-capacity-ethanol-Cp-Cv-isobaric-isochoric-ethyl-alcohol-d_2030.html

    var heat_capacity = (1-(alcohol_percent/100.0))*water_heat_capacity+(alcohol_percent/100.0)*ethanol_heat_capacity

    return heat_capacity

}

// main
function calc_freeze() {

    // parse input
    let {start_temp, end_temp, freezer_temp, volume, area, alcohol_percent, container_material} = parse_input();

    if (start_temp < end_temp){
        document.getElementById("freeze_time").innerHTML = "Error: start temperature is lower than desired temperature!";

        return
    }

    // set alhohol content to zero if empty field
    if (isNaN(alcohol_percent)){
        alcohol_percent = 0
    }

    if (container_material == "aluminum") {
        var heat_transfer = 33.3 
    }

    if (container_material == "plastic") {
        var heat_transfer = 7.5 
    }

    if (container_material == "glass") {
        var heat_transfer = 4.5 
    }

    var heat_capacity = calc_heat_capacity(alcohol_percent) * (volume/1000.0) // converting from specific heat capacity to heat capacity, assuming density of 1g/mL

    var k = heat_transfer * (area/heat_capacity); //

    var freeze_time = (-(Math.log((end_temp - freezer_temp)/(start_temp - freezer_temp))/(k))) / 60.0;  // in minutes

    console.log("Calculated values:")
    console.log("heat_capacity: " + heat_capacity)
    console.log("heat_transfer: " + heat_transfer)
    console.log("k: " + k)
    console.log("freeze_time: " + freeze_time)
    console.log("####################################")

document.getElementById("freeze_time").innerHTML = "<b>Result:</b> " + Math.round(freeze_time) + " minutes";

}
