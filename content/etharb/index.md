---
title: ETHARB - Ethereum-Fiat Arbitrage analysis
description: Summer 2021 - present
---

Exploring Ethereum-fiat arbitrage trading oportunities on the Kraken cryptocurrency exchange when funds run dry, especially the ETH-EUR pair on weekends. Under development. 

Source can be found here: [GitLab Source with Jupyter notebook](https://gitlab.com/philip-jakobsen/etharb).

__Competencies developed:__ Python programming, time series data analysis, data visualization and WebSocket connections. 
